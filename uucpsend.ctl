#  Control file for uucpsend.
#
# Format:
#	site:queuesize:max_size:header:compressor:[<args...>]
#	<site>		The name used in the newsfeeds file for this site;
#			this determines the name of the batchfile, etc.
#       <queuesize>	Size of all batches for this system.
#	<max_size>	Maximum size of one batch that is sent to this
#			system, see batcher(8).
#	<header>	The string that written in the first line of
#			the batch.
#	<compressor>	A program that should be used to compress each
#			batch.
#	<args>		Other args to pass to uux.
#
#  Everything after the pound sign is ignored.
#
# The default entry is very important. It's the only entry that is really 
# needed.
#

# First set defaults for all sites. These may be overwritten.
#
/default/:20:200:cunbatch:compress:-r -n

#tuonela:200:100:gunbatch:gzip:-r -n
#escher:2000:300:cunbatch:gzip:-n
