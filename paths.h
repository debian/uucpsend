/*
   Copyright (c) 1995-8,2001  Martin Schulze <joey@infodrom.org>

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111, USA.
*/

/*
**  Define paths for uucpsend
*/

#ifndef __paths_h__
#define __paths_h__

/*  All of the following defines are originally made in
**  ~inn-1.5/include/paths.h. This is only a good asumption, it would
**  be better to use the original though.
*/

#define _PATH_LOCKS	"/var/run/innd"
#define _PATH_BATCHDIR	"/var/spool/news/out.going"
#define _PATH_NEWSBIN	"/usr/lib/news/bin"
#define _PATH_MOST_LOGS	"/var/log/news"

/*
** End of copy
*/

#define PATH_CTLINND    _PATH_NEWSBIN "/ctlinnd"
#define PATH_MV         "/bin/mv"
#define PATH_CAT        "/bin/cat"
#define PATH_RM         "/bin/rm"
#define PATH_DF         "/bin/df"
#define PATH_DU         "/usr/bin/du"
#define PATH_TOUCH      "/usr/bin/touch"
#define PATH_SORT       "/usr/bin/sort"
#define PATH_UUX        "/usr/bin/uux"
#define PATH_UUSPOOL    "/var/spool/uucp"
#define PATH_UUCPCTL	"/etc/news/uucpsend.ctl"

#endif /* __paths_h__ */
